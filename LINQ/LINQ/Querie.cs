﻿using LINQ.Models;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Timer = System.Threading.Timer;

namespace LINQ
{
    public class Querie
    {
        public static HttpClient client = new HttpClient();
        public static Timer timer = null;
        private AsyncLock _asyncLock = new AsyncLock();

        readonly TaskCompletionSource<int> tcs = new TaskCompletionSource<int>();
        public Querie()
        {
            client.BaseAddress = new Uri(Route.Global);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }
        public async Task<int> MarkRandomTaskWithDelay(int delay)
        {
            int id = 0;

            timer = new Timer(async obj =>
            {
                using (await _asyncLock.LockAsync())
                {
                    List<Tasks> tasks = await GetAllTasks();
                    Random random = new Random();
                    var maxValue = tasks.Max(item => item.id);
                    id = random.Next(1, maxValue+1);
                    int result = await ChangeTask(id);
                    if (result == -1)
                    {
                        tcs.SetException(new ArgumentNullException("Task not found"));
                    }
                    else
                    {
                        Console.WriteLine($"Task {result} marked as completed");
                        tcs.TrySetResult(result);
                    }

                }
            }, null, delay, -1);

            return await tcs.Task;
        }

        protected static async Task<List<Tasks>> GetAllTasks()
        {
            var response = await client.GetAsync(Route.Tasks);
            var content = await response.Content.ReadAsStringAsync();
            List<Tasks> result = JsonConvert.DeserializeObject<List<Tasks>>(content);
            return result;
        }

        protected static async Task<int> ChangeTask(int Id)
        {
            var response = await client.GetAsync(Route.Tasks + "changeState/" + Id);
            var content = await response.Content.ReadAsStringAsync();
            int result = JsonConvert.DeserializeObject<int>(content);
            return result;
        }


    }
}
