﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO.LinqModels
{
    public class CountOfTasksInProjectsDTO
    {
        public ProjectDTO Project { get; set; }
        public int CountOfTasks { get; set; }
    }
}
