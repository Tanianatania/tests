﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO.LinqModels
{
    public class UserWithTasksDTO
    {
        public UserDTO User { get; set; }
        public List<TasksDTO> Tasks { get; set; }
    }
}
