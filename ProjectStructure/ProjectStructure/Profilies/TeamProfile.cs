﻿using AutoMapper;
using BLL.DTO;
using DAL.Entities;

namespace ProjectStructure.Profilies
{
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<TeamDTO, Team>();
            CreateMap<Team, TeamDTO>();
        }
    }
}
