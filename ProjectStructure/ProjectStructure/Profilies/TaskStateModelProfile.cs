﻿using AutoMapper;
using BLL.DTO;
using DAL.Entities;

namespace ProjectStructure.Profilies
{
    public class TaskStateModelProfile : Profile
    {
        public TaskStateModelProfile()
        {
            CreateMap<TaskStateModel, TaskStateModelDTO>();
            CreateMap<TaskStateModelDTO, TaskStateModel>();
        }
    }
}