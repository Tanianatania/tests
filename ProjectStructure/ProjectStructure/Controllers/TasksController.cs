﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService _service;

        public TasksController(ITaskService service)
        {
            _service = service;
        }
        // GET: api/Tasks
        [HttpGet]
        public async Task<IEnumerable<TasksDTO>> Get()
        {
            return await _service.GetList();
        }

        // GET: api/Tasks/5
        [HttpGet("{id}", Name = "GetTasks")]
        public async Task<TasksDTO> Get(int id)
        {
            return await _service.Get(id);
        }

        [Route("changeState/{id}")]
        public async Task<int> ChangeState(int id)
        {
            return await _service.ChangeStateById(id);
        }

        // POST: api/Tasks
        [HttpPost]
        public async Task Post([FromBody] string value)
        {
            TasksDTO item = JsonConvert.DeserializeObject<TasksDTO>(value);
           await _service.Create(item);
        }

        // PUT: api/Tasks
        [HttpPut]
        public async Task Put([FromBody] string value)
        {
            TasksDTO item = JsonConvert.DeserializeObject<TasksDTO>(value);
            await _service.Update(item);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _service.Delete(id);
        }
    }
}
