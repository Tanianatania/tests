﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IService<ProjectDTO> _service;
        public ProjectsController(IService<ProjectDTO> service)
        {
            _service = service;
        }
        // GET: api/Projects
        [HttpGet]
        public async Task<IEnumerable<ProjectDTO>> Get()
        {
            return await _service.GetList();
        }

        // GET: api/Projects/5
        [HttpGet("{id}", Name = "GetProject")]
        public async Task<ProjectDTO> Get(int id, CancellationToken cancellationToken)
        {
            return await _service.Get(id);
        }

        // POST: api/Projects
        [HttpPost]
        public async Task Post([FromBody] string value, CancellationToken cancellationToken)
        {
            ProjectDTO item = JsonConvert.DeserializeObject<ProjectDTO>(value);
            await _service.Create(item);
        }

        // PUT: api/Projects/5
        [HttpPut]
        public async Task Put([FromBody] string value,CancellationToken cancellationToken)
        {
            ProjectDTO item = JsonConvert.DeserializeObject<ProjectDTO>(value);
           await _service.Update(item);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task Delete(int id, CancellationToken cancellationToken)
        {
            await _service.Delete(id);
        }
    }
}
