﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.DTO;
using BLL.DTO.LinqModels;
using BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinqController : ControllerBase
    {
        private readonly ILinqService _service;

        public LinqController(ILinqService service)
        {
            _service = service;
        }

        [Route("getCountOfTaskInProjectByUserId/{id}")]
        [HttpGet]
        public async Task<List<CountOfTasksInProjectsDTO>> GetCountOfTaskInProjectByUserId(int id)
        {
            return (await _service.GetCountOfTaskInProjectByUserId(id)).ToList();
        }

        [Route("getByProjectId/{id}")]
        [HttpGet]
        public async Task<ProjectInformationDTO> GetByProjectId(int id)
        {
            return await _service.GetByProjectId(id);
        }

        [Route("getByUserId/{id}")]
        [HttpGet]
        public async Task<UserInformationDTO> GetByUserId(int id)
        {
           return await _service.GetByUserId(id);
        }

        [Route("getFinishesTasks/{id}")]
        [HttpGet]
        public async Task<IEnumerable<FinishedTasksDTO>> GetFinishedTasks(int id)
        {
            return await _service.GetFinishedTasks(id);
        }

        [Route("getSortedList")]
        [HttpGet]
        public async Task<IEnumerable<UserWithTasksDTO>> GetSortedListByUserAndTeam()
        {
            return await _service.GetSortedListByUserAndTeam();
        }

        [Route("getAllFromFile")]
        public async Task<IEnumerable<ResultModelDTO>> GetAllFromFile()
        {
            return (await _service.GetAllFromFile()).OrderByDescending(a=>a.Date);
        }

        [Route("getTasksOfUser/{id}")]
        [HttpGet]
        public async Task<IEnumerable<TasksDTO>> GetTaskOfUser(int id)
        {
            return await _service.GetTaskOfUser(id);
        }

        [Route("getTeamWithYoungUsers")]
        [HttpGet]
        public async Task<IEnumerable<TeamWithUserDTO>> GetTeamWithYoungUsers()
        {
            return await _service.GetTeamWithYoungUser();
        }
    }
}