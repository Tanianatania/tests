﻿using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class TaskStateModel
    {
        public int Id { get; set; }
        [MaxLength(10)]
        public string Value { get; set; }
    }
}
