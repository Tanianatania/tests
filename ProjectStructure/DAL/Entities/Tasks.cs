﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.Entities
{
    public class Tasks
    {
        public int Id { get; set; }
        [MaxLength(15)]
        public string Name { get; set; }
        [MaxLength(50)]
        [MinLength(10)]
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public int State { get; set; }
        [Required]
        public int ProjectId { get; set; }
        public virtual Project Project { get;set;}
        [Required]
        public int PerformerId { get; set; }
        public virtual User Performer { get;set;}

        public override string ToString()
        {
            return $"Id: {Id}, Name: {Name}, Description: {Description.Replace("\n"," ")}, Create at: {CreatedAt}, Finished at: {FinishedAt}" +
                $" State: {State}, Project Id: {ProjectId}, Perfomer Id: {PerformerId}\n";
        }
    }
}
