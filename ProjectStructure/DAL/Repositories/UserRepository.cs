﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Interfaces;
using DAL.Entities;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private readonly ApplicationDbContext _context;

        public UserRepository(ApplicationDbContext context)
        {
            _context = context;
            _disposed = false;
        }

        public Task<List<User>> GetList()
        {
            return _context.Users
                .Include(a=>a.Team)
                .ToListAsync();
        }

        public  Task<User> Get(int id)
        {
            return  _context.Users.FirstOrDefaultAsync(a => a.Id == id);
        }

        public async Task Create(User item)
        {
            await _context.Users.AddAsync(item);
        }

        public async Task Update(User item)
        {
            var oldTasks = await _context.Users.FirstOrDefaultAsync(a => a.Id == item.Id);
            if (oldTasks != null)
            {
               _context.Users.Update(item);
            }
        }

        public async Task Delete(int id)
        {
            var oldTasks = await _context.Users.FirstOrDefaultAsync(a => a.Id == id);
            if (oldTasks != null)
            {
                _context.Users.Remove(oldTasks);
            }
        }

        public async Task Save()
        {
            await _context.SaveChangesAsync();
        }

        #region Dispose pattern

        private bool _disposed;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
