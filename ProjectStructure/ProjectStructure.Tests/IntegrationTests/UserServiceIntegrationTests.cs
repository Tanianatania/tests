﻿using AutoMapper;
using BLL.DTO;
using BLL.Hubs;
using BLL.Services;
using DAL;
using DAL.Entities;
using DAL.Repositories;
using FakeItEasy;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using SharedQueueService.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.Tests.IntegrationTests
{
    class UserServiceIntegrationTests
    {
        [Test]
        [TestCase(1)]
        public async Task User_delete_test_Should_delete_user_when_given_id_existing_user(int Id)
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Delete_user")
                .Options;

            var context = new ApplicationDbContext(options);
            context.Users.Add(new User
            {
                Id = 1,
                FirstName = "Sasha",
                LastName = "Vovk",
                Email = "sasha.vovk@gmail.com",
                Birthday = new DateTime(6, 6, 6, 12, 12, 12),
                RegisteredAt = new DateTime(8, 10, 11, 17, 18, 19),
                TeamId = 1
            });
            context.SaveChanges();

            var repository = new UserRepository(context);

            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<User, UserDTO>();
                cfg.CreateMap<UserDTO, User>();
            });

            var mapper = mapperConfig.CreateMapper();
            var messageProducerScopeFactory = A.Fake<IMessageProducerScopeFactory>();
            var messageConsumerScopeFactory = A.Fake<IMessageConsumerScopeFactory>();
            var hub = A.Fake<IHubContext<ProjectHub>>();
            var service = new UserService(repository,
                messageProducerScopeFactory,
                messageConsumerScopeFactory,
                hub,
                mapper);

            await service.Delete(Id);
            var result = (await service.GetList()).ToList();

            Assert.That(0, Is.EqualTo(result.Count));

        }
    }
}
