﻿using AutoMapper;
using BLL.DTO;
using BLL.Hubs;
using BLL.Services;
using DAL;
using DAL.Entities;
using DAL.Repositories;
using FakeItEasy;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using SharedQueueService.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.Tests.IntegrationTests
{
    class TaskServiceIntegrationTests
    {
        [Test]
        [TestCase(1)]
        public async Task Task_delete_test_Should_delete_user_when_given_id_existing_user(int Id)
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                            .UseInMemoryDatabase(databaseName: "Delete_task")
                            .Options;

            var context = new ApplicationDbContext(options);
            context.Tasks.Add(new Tasks
            {
                Id=1,
                Name = "Test task",
                CreatedAt = new DateTime(10, 10, 10, 10, 10, 10),
                Description = "Task for delete test",
                FinishedAt = new DateTime(11, 11, 11, 11, 11, 11),
                PerformerId = 1,
                ProjectId = 1
            });
            context.SaveChanges();

            var repository = new TaskRepository(context);

            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Tasks, TasksDTO>();
                cfg.CreateMap<TasksDTO, Tasks>();
            });

            var mapper = mapperConfig.CreateMapper();
            var messageProducerScopeFactory = A.Fake<IMessageProducerScopeFactory>();
            var messageConsumerScopeFactory = A.Fake<IMessageConsumerScopeFactory>();
            var hub = A.Fake<IHubContext<ProjectHub>>();
            var service = new TaskService(repository,
                messageProducerScopeFactory,
                messageConsumerScopeFactory,
                hub,
                mapper);

            await service.Delete(Id);
            var result = (await service.GetList()).ToList();

            Assert.That(0, Is.EqualTo(result.Count));
        }
    }
}
