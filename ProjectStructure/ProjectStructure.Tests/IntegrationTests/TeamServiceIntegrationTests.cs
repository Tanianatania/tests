﻿using AutoMapper;
using BLL.DTO;
using BLL.Hubs;
using BLL.Services;
using DAL;
using DAL.Entities;
using DAL.Repositories;
using FakeItEasy;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using SharedQueueService.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.Tests.IntegrationTests
{
    class TeamServiceIntegrationTests
    {
        [Test]
        public async Task Create_team_test_Should_create_new_project_when_given_correct_data()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Create_team")
                .Options;

            var context = new ApplicationDbContext(options);

            var repository = new TeamRepository(context);

            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Team, TeamDTO>();
                cfg.CreateMap<TeamDTO, Team>();
            });

            var mapper = mapperConfig.CreateMapper();
            var messageProducerScopeFactory = A.Fake<IMessageProducerScopeFactory>();
            var messageConsumerScopeFactory = A.Fake<IMessageConsumerScopeFactory>();
            var hub = A.Fake<IHubContext<ProjectHub>>();
            var service = new TeamService(repository,
                messageProducerScopeFactory,
                messageConsumerScopeFactory,
                hub,
                mapper);

            var project = new TeamDTO
            {
                Name = "Test Team"
            };
            await service.Create(project);
            var result = await service.GetList();
            var team = await service.Get(1);

            Assert.That(team.Name, Is.EqualTo("Test Team"));
            Assert.That(1, Is.EqualTo(result.ToList().Count));
        }
    }
}
