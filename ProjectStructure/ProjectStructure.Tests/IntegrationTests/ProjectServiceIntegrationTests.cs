﻿using AutoMapper;
using BLL.DTO;
using BLL.Hubs;
using BLL.Services;
using DAL;
using DAL.Entities;
using DAL.Repositories;
using FakeItEasy;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using SharedQueueService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.Tests.IntegrationTests
{
    public class ProjectServiceIntegrationTests
    {
        [Test]
        [Obsolete]
        public async Task Create_project_test_Should_create_new_project_when_given_correct_data()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Create_project")
                .Options;

            var context = new ApplicationDbContext(options);

            var repository = new ProjectRepository(context);

            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Project, ProjectDTO>();
                cfg.CreateMap<ProjectDTO, Project>();
            });

            var mapper = mapperConfig.CreateMapper();
            var messageProducerScopeFactory = A.Fake<IMessageProducerScopeFactory>();
            var messageConsumerScopeFactory = A.Fake<IMessageConsumerScopeFactory>();
            var hub = A.Fake<IHubContext<ProjectHub>>();
            var service = new ProjectService(repository,
                messageProducerScopeFactory,
                messageConsumerScopeFactory,
                hub,
                mapper);

            var project = new ProjectDTO
            {
                Id = 1,
                Name = "Project 1",
                Description = "It is project 1",
                CreatedAt = DateTime.Now.AddDays(-12),
                Deadline = DateTime.Now.AddDays(12),
                AuthorId = 1,
                TeamId = 1
            };
            await service.Create(project);
            var result = await service.GetList();
            Assert.That(1, Is.EqualTo(result.ToList().Count));
        }
    }
}
