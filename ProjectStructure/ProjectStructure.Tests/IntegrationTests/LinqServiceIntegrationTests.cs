﻿using AutoMapper;
using BLL.DTO;
using BLL.Hubs;
using BLL.Services;
using DAL;
using DAL.Entities;
using DAL.Repositories;
using FakeItEasy;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using SharedQueueService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.Tests.IntegrationTests
{
    public class LinqServiceIntegrationTests
    {
        [Test]
        [TestCase(1)]
        public async Task GetFinishedTest_Should_return_list_of_tasks_when_given_id_existing_user(int Id)
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                            .UseInMemoryDatabase(databaseName: "Get_finished_task")
                            .Options;

            var context = new ApplicationDbContext(options);
            context.Tasks.Add(new Tasks
            {
                Name = "Test task1",
                CreatedAt = new DateTime(2010, 10, 10, 10, 10, 10),
                Description = "Task for delete test",
                State=3,
                FinishedAt = new DateTime(2011, 11, 11, 11, 11, 11),
                PerformerId = 1,
                ProjectId = 1
            });
            context.Tasks.Add(new Tasks
            {
                Name = "Test task2",
                CreatedAt = new DateTime(2010, 10, 10, 10, 10, 10),
                Description = "Task for delete test",
                State = 3,
                FinishedAt = new DateTime(2019, 11, 11, 11, 11, 11),
                PerformerId = 1,
                ProjectId = 1
            });
            context.SaveChanges();

            var taskRepository = new TaskRepository(context);
            var projectRepository = new ProjectRepository(context);
            var userRepository = new UserRepository(context);
            var teamRepository = new TeamRepository(context);

            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Tasks, TasksDTO>();
                cfg.CreateMap<TasksDTO, Tasks>();
                cfg.CreateMap<User, UserDTO>();
                cfg.CreateMap<UserDTO, User>();
                cfg.CreateMap<Team, TeamDTO>();
                cfg.CreateMap<TeamDTO, Team>();
                cfg.CreateMap<Project, ProjectDTO>();
                cfg.CreateMap<ProjectDTO, Project>();
            });

            var mapper = mapperConfig.CreateMapper();
            var messageProducerScopeFactory = A.Fake<IMessageProducerScopeFactory>();
            var messageConsumerScopeFactory = A.Fake<IMessageConsumerScopeFactory>();
            var hub = A.Fake<IHubContext<ProjectHub>>();
            var service = new LinqService(projectRepository,
                taskRepository,
                userRepository,
                teamRepository,
                messageProducerScopeFactory,
                messageConsumerScopeFactory,
                hub,
                mapper);

            var result = (await service.GetFinishedTasks(Id)).ToList();

            Assert.That(1, Is.EqualTo(result.Count));
            Assert.That(result[0].Id, Is.EqualTo(2));
            Assert.That(result[0].Name, Is.EqualTo("Test task2"));
        }
    }
}
