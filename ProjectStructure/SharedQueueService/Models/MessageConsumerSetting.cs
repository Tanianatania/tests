﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharedQueueService.Models
{
    public class MessageConsumerSetting
    {
        public bool SequentialFetch { get; set; } = true;
        public bool AutoActnowledge { get; set; } = false;
        public IModel Channel { get; set; }
        public string QueueName { get; set; }

    }
}
