﻿using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharedQueueService.Interfaces
{
    public interface IMessageConsumer
    {
        void Connect();
        void SetActnowledge(ulong deliveryTag, bool processe);

        event EventHandler<BasicDeliverEventArgs> Received;
    }
}
