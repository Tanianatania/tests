﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharedQueueService.Interfaces
{
    public interface IMessageQueue:IDisposable
    {
        IModel Channel { get;}
        void DeclareExchange(string exchangeName, string exchangeType);
        void BinaQueue(string exchangeName, string routingKey, string queueName);
    }
}
