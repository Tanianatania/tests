﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedQueueService.Interfaces
{
    public interface IMessageConsumerScope:IDisposable
    {
        IMessageConsumer MessageConsumer { get; }
    }
}
